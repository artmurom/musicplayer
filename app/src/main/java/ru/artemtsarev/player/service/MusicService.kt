package ru.artemtsarev.player.service

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.app.*
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.media.AudioFocusRequest
import android.media.AudioManager
import android.net.Uri
import android.os.Binder
import android.os.Build
import android.os.IBinder
import android.support.annotation.RequiresApi
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaButtonReceiver
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.extractor.ExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.DataSource
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import ru.artemtsarev.player.App
import ru.artemtsarev.player.R
import ru.artemtsarev.player.model.Song
import ru.artemtsarev.player.repository.SongRepository
import ru.artemtsarev.player.screen.current.CurrentActivity
import javax.inject.Inject


class MusicService : Service() {

    companion object {
        const val NOTIFICATION_ID = 111
        const val NOTIFICATION_CHANNEL_ID = "channel"
    }

    private val mMetadata: MediaMetadataCompat.Builder = MediaMetadataCompat.Builder()
    private val mStateBuilder: PlaybackStateCompat.Builder = PlaybackStateCompat.Builder().setActions(
            PlaybackStateCompat.ACTION_PLAY
                    or PlaybackStateCompat.ACTION_STOP
                    or PlaybackStateCompat.ACTION_PAUSE
                    or PlaybackStateCompat.ACTION_PLAY_PAUSE
    )


    private lateinit var mMediaSession: MediaSessionCompat

    @Inject
    lateinit var musicRepository: SongRepository

    private lateinit var mManager: AudioManager
    private lateinit var mAudioFocus: AudioFocusRequest
    private var mIsAudioFocusRequested = false

    private lateinit var mPlayer: SimpleExoPlayer
    private lateinit var mExtractorsFactory: ExtractorsFactory
    private lateinit var mDataSourceFactory: DataSource.Factory
    private var durationSet = false

    private var mLastPosition: Long = 0


    @SuppressLint("WrongConstant")
    @RequiresApi(Build.VERSION_CODES.O)
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    override fun onCreate() {
        super.onCreate()

        app.component.inject(this)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val notificationChannel = NotificationChannel(NOTIFICATION_CHANNEL_ID,
                    getString(R.string.notification_channel),
                    NotificationManagerCompat.IMPORTANCE_DEFAULT)
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(notificationChannel)

            val audioAttributes = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build()
            mAudioFocus = AudioFocusRequest.Builder(AudioManager.AUDIOFOCUS_GAIN)
                    .setOnAudioFocusChangeListener(mAudioFocusChangeListener)
                    .setAcceptsDelayedFocusGain(false)
                    .setWillPauseWhenDucked(true)
                    .setAudioAttributes(audioAttributes)
                    .build()
        }

        mManager = getSystemService(Context.AUDIO_SERVICE) as AudioManager

        mMediaSession = MediaSessionCompat(this, "PlayerService")
        mMediaSession.setFlags(MediaSessionCompat.FLAG_HANDLES_MEDIA_BUTTONS or MediaSessionCompat.FLAG_HANDLES_TRANSPORT_CONTROLS)
        mMediaSession.setCallback(mMediaSessionCallback)

        val appContext = applicationContext

        val activityIntent = Intent(appContext, CurrentActivity::class.java)
        mMediaSession.setSessionActivity(PendingIntent.getActivity(appContext, 0, activityIntent, 0))

        val mediaButtonIntent = Intent(Intent.ACTION_MEDIA_BUTTON, null, appContext, MediaButtonReceiver::class.java)
        mMediaSession.setMediaButtonReceiver(PendingIntent.getBroadcast(appContext, 0, mediaButtonIntent, 0))

        mPlayer = ExoPlayerFactory.newSimpleInstance(DefaultRenderersFactory(this), DefaultTrackSelector(), DefaultLoadControl())
        mPlayer.addListener(mPlayerListener)

        val bandWidthMeter = DefaultBandwidthMeter()
        mDataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, "exoplayer2"), bandWidthMeter)
        mExtractorsFactory = DefaultExtractorsFactory()
    }

    val app: App
        get() = application as App

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        MediaButtonReceiver.handleIntent(mMediaSession, intent)
        return super.onStartCommand(intent, flags, startId)
    }

    override fun onDestroy() {
        super.onDestroy()
        mMediaSession.release()
        mPlayer.release()
    }

    override fun onBind(intent: Intent?): IBinder {
        return PlayerServiceBinder()
    }

    inner class PlayerServiceBinder : Binder() {
        val mediaSessionToken: MediaSessionCompat.Token
            get() = mMediaSession.sessionToken
    }


    private val mMediaSessionCallback: MediaSessionCompat.Callback = object : MediaSessionCompat.Callback() {

        var currentUri: Uri? = null
        var currentState = PlaybackStateCompat.STATE_STOPPED


        override fun onPlay() {
            if (!mPlayer.playWhenReady) {
                startService(Intent(applicationContext, MusicService::class.java))

                val song = musicRepository.current
                updateMetadataFromSong(song)

                prepareToPlay(Uri.parse(song.previewUrl))

                if (!mIsAudioFocusRequested) {
                    mIsAudioFocusRequested = true

                    val audioFocusResult: Int = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        mManager.requestAudioFocus(mAudioFocus)
                    } else {
                        mManager.requestAudioFocus(mAudioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN)
                    }
                    if (audioFocusResult != AudioManager.AUDIOFOCUS_REQUEST_GRANTED)
                        return
                }

                mMediaSession.isActive = true

                registerReceiver(mBecomingNoisyReceiver, IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY))

                mPlayer.playWhenReady = true
            }

            mMediaSession.setPlaybackState(mStateBuilder.setState(PlaybackStateCompat.STATE_PLAYING, mLastPosition, 1F).build())

            mPlayer.seekTo(mLastPosition)


            currentState = PlaybackStateCompat.STATE_PLAYING

            refreshNotificationAndForegroundStatus(currentState)
        }


        override fun onPause() {
            if (mPlayer.playWhenReady) {
                mPlayer.playWhenReady = false
                unregisterReceiver(mBecomingNoisyReceiver)
            }

            if (mIsAudioFocusRequested) {
                mIsAudioFocusRequested = false

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    mManager.abandonAudioFocusRequest(mAudioFocus)
                } else {
                    mManager.abandonAudioFocus(mAudioFocusChangeListener)
                }
            }

            mLastPosition = mPlayer.currentPosition

            mMediaSession.setPlaybackState(mStateBuilder.setState(PlaybackStateCompat.STATE_PAUSED, PlaybackStateCompat.PLAYBACK_POSITION_UNKNOWN, 1F).build())
            currentState = PlaybackStateCompat.STATE_PAUSED

            refreshNotificationAndForegroundStatus(currentState)
        }


        override fun onStop() {
            if (mPlayer.playWhenReady) {
                mPlayer.playWhenReady = false
                unregisterReceiver(mBecomingNoisyReceiver)
            }

            if (mIsAudioFocusRequested) {
                mIsAudioFocusRequested = false

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    mManager.abandonAudioFocusRequest(mAudioFocus)
                } else {
                    mManager.abandonAudioFocus(mAudioFocusChangeListener)
                }
            }

            mMediaSession.isActive = false

            mLastPosition = 0


            mMediaSession.setPlaybackState(mStateBuilder.setState(PlaybackStateCompat.STATE_STOPPED, mLastPosition, 1F).build())
            currentState = PlaybackStateCompat.STATE_STOPPED

            refreshNotificationAndForegroundStatus(currentState)

            stopSelf()
        }


        override fun onSeekTo(pos: Long) {
            mPlayer.seekTo(pos)
            mMediaSession.setPlaybackState(mStateBuilder.setState(currentState, pos, 1F).build())
            mLastPosition = pos
        }


        fun prepareToPlay(uri: Uri) {
            if (uri != currentUri) {
                currentUri = uri
                val mediaSource = ExtractorMediaSource(uri, mDataSourceFactory, mExtractorsFactory, null, null)
                mPlayer.prepare(mediaSource)
            }
        }

        fun updateMetadataFromSong(song: Song) {
            mMetadata.putBitmap(MediaMetadataCompat.METADATA_KEY_ART, BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            mMetadata.putString(MediaMetadataCompat.METADATA_KEY_ART_URI, song.artworkUrl100)
            mMetadata.putString(MediaMetadataCompat.METADATA_KEY_TITLE, song.trackName)
            mMetadata.putString(MediaMetadataCompat.METADATA_KEY_ARTIST, song.artistName)
            mMetadata.putLong(MediaMetadataCompat.METADATA_KEY_DURATION, mPlayer.duration)
            mMediaSession.setMetadata(mMetadata.build())
        }

    }

    private val mBecomingNoisyReceiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            if (AudioManager.ACTION_AUDIO_BECOMING_NOISY == intent.action) {
                mMediaSessionCallback.onPause()
            }
        }
    }

    private val mAudioFocusChangeListener = AudioManager.OnAudioFocusChangeListener { focusChange ->
        when (focusChange) {
            AudioManager.AUDIOFOCUS_GAIN -> mMediaSessionCallback.onPlay()
            AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK -> mMediaSessionCallback.onPause()
            else -> mMediaSessionCallback.onPause()
        }
    }


    private val mPlayerListener = object : ExoPlayer.EventListener {

        override fun onTracksChanged(trackGroups: TrackGroupArray?, trackSelections: TrackSelectionArray?) {
            durationSet = false
            mLastPosition = 0
        }

        override fun onTimelineChanged(timeline: Timeline?, manifest: Any?) {
        }


        override fun onLoadingChanged(isLoading: Boolean) {
        }


        override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
            if (playbackState == ExoPlayer.STATE_READY && !durationSet) {
                durationSet = true
                mMetadata.putLong(MediaMetadataCompat.METADATA_KEY_DURATION, mPlayer.duration)
                mMediaSession.setMetadata(mMetadata.build())
            }

            if (playbackState == ExoPlayer.STATE_ENDED) {


                mPlayer.seekTo(0)

                mMediaSessionCallback.onPause()
            }
        }


        override fun onPlayerError(error: ExoPlaybackException) {
        }


        override fun onPositionDiscontinuity() {
        }


        override fun onPlaybackParametersChanged(playbackParameters: PlaybackParameters?) {

        }
    }


    private fun refreshNotificationAndForegroundStatus(playbackState: Int) {
        when (playbackState) {
            PlaybackStateCompat.STATE_PLAYING -> {
                startForeground(NOTIFICATION_ID, getNotification(playbackState))
            }
            PlaybackStateCompat.STATE_PAUSED -> {
                NotificationManagerCompat.from(this).notify(NOTIFICATION_ID, getNotification(playbackState))
                stopForeground(false)
            }
            else -> {
                stopForeground(true)
            }
        }
    }


    private fun getNotification(playbackState: Int): Notification {
        val builder = NotificationWidget.create(this, mMediaSession, NOTIFICATION_CHANNEL_ID)

        if (playbackState == PlaybackStateCompat.STATE_PLAYING)
            builder.addAction(NotificationCompat.Action(android.R.drawable.ic_media_pause, getString(R.string.song_pause), MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_PAUSE)))
        else
            builder.addAction(NotificationCompat.Action(android.R.drawable.ic_media_play, getString(R.string.song_play), MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_PLAY)))

        builder.setStyle(android.support.v4.media.app.NotificationCompat.MediaStyle()
                .setShowActionsInCompactView(0)
                .setShowCancelButton(true)
                .setCancelButtonIntent(MediaButtonReceiver.buildMediaButtonPendingIntent(this, PlaybackStateCompat.ACTION_STOP))
                .setMediaSession(mMediaSession.sessionToken))
        builder.setSmallIcon(R.mipmap.ic_launcher)
        builder.setShowWhen(false)
        builder.priority = NotificationCompat.PRIORITY_MAX
        builder.setOnlyAlertOnce(true)

        return builder.build()
    }


}