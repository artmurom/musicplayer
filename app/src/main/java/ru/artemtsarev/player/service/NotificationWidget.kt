package ru.artemtsarev.player.service

import android.content.Context
import android.support.v4.app.NotificationCompat
import android.support.v4.media.session.MediaButtonReceiver
import android.support.v4.media.session.MediaSessionCompat
import android.support.v4.media.session.PlaybackStateCompat

class NotificationWidget {

    companion object {
        fun create(context: Context, mediaSession: MediaSessionCompat, channelId: String): NotificationCompat.Builder {
            val controller = mediaSession.controller
            val metaData = controller.metadata
            val description = metaData.description


            return NotificationCompat.Builder(context, channelId)
                    .setContentTitle(description.title)
                    .setContentText(description.subtitle)
                    .setSubText(description.description)
                    .setLargeIcon(description.iconBitmap)
                    .setContentIntent(controller.sessionActivity)
                    .setDeleteIntent(MediaButtonReceiver.buildMediaButtonPendingIntent(context, PlaybackStateCompat.ACTION_STOP))
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
        }
    }
}