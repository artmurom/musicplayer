package ru.artemtsarev.player.di

import dagger.Component
import ru.artemtsarev.player.App
import ru.artemtsarev.player.screen.musics.MusicComponent
import ru.artemtsarev.player.screen.musics.MusicPresenterModule
import ru.artemtsarev.player.service.MusicService
import javax.inject.Singleton


@Singleton
@Component(modules = [(DataModule::class)])
interface AppComponent {
    fun inject(app: App)

    fun inject(musicService: MusicService)

    fun plus(musicPresenterModule: MusicPresenterModule): MusicComponent

}
