package ru.artemtsarev.player.di

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.artemtsarev.player.App
import ru.artemtsarev.player.BuildConfig
import ru.artemtsarev.player.network.SearchService
import ru.artemtsarev.player.repository.DefaultSongRepository
import ru.artemtsarev.player.repository.SongRepository
import javax.inject.Singleton


@Module
class DataModule(val app: App) {


    @Provides
    @Singleton
    fun provideSongRepository(searchService: SearchService): SongRepository {
        return DefaultSongRepository(searchService)
    }

    @Provides
    @Singleton
    fun provideApp() = app


    @Provides
    @Singleton
    fun provideNewsService(retrofit: Retrofit): SearchService {
        return retrofit.create<SearchService>(SearchService::class.java)
    }


    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
                .baseUrl(BuildConfig.API_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
    }
}
