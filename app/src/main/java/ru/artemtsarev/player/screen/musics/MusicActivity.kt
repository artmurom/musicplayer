package ru.artemtsarev.player.screen.musics

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import android.os.IBinder
import android.os.RemoteException
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.view.Menu
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_music.*
import ru.artemtsarev.player.App
import ru.artemtsarev.player.R
import ru.artemtsarev.player.model.DefaultRequest
import ru.artemtsarev.player.model.Song
import ru.artemtsarev.player.screen.current.CurrentActivity
import ru.artemtsarev.player.service.MusicService
import javax.inject.Inject


class MusicActivity : AppCompatActivity(), MusicAdapter.OnSongClickListner, MusicContract.View, SearchView.OnQueryTextListener {


    private lateinit var mAdapter: MusicAdapter

    private var mBinder: MusicService.PlayerServiceBinder? = null
    private var mController: MediaControllerCompat? = null

    @Inject
    lateinit var mPresenter: MusicPresenter

    private val mServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            mBinder = service as MusicService.PlayerServiceBinder
            mController = try {
                MediaControllerCompat(this@MusicActivity, mBinder!!.mediaSessionToken)
            } catch (e: RemoteException) {
                null
            }

        }

        override fun onServiceDisconnected(name: ComponentName) {
            mBinder = null
            mController = null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music)

        component.inject(this)

        mAdapter = MusicAdapter(this)
        val layoutManager = LinearLayoutManager(this)

        rvMusic.adapter = mAdapter
        rvMusic.layoutManager = layoutManager
        rvMusic.addItemDecoration(DividerItemDecoration(this,
                DividerItemDecoration.VERTICAL))

        bindService(Intent(this, MusicService::class.java), mServiceConnection, BIND_AUTO_CREATE)

    }


    val AppCompatActivity.app: App
        get() = application as App

    private val component by lazy {
        app.component.plus(MusicPresenterModule(this))
    }

    override fun onSongClick(song: Song) {
        mPresenter.clickSong(song)
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        val searchItem = menu.findItem(R.id.search)
        val searchView = searchItem.actionView as SearchView

        searchView.setOnQueryTextListener(this)

        return true
    }

    override fun onQueryTextSubmit(query: String): Boolean {
        mPresenter.search(query)
        return true
    }


    public override fun onDestroy() {
        super.onDestroy()
        unbindService(mServiceConnection)
    }


    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }

    override fun playSong(song: Song) {
        if (mController!!.playbackState != null && mController!!.playbackState.state == PlaybackStateCompat.STATE_PLAYING) {
            mController!!.transportControls.stop()
            mController!!.transportControls.play()
        } else {
            mController!!.transportControls.play()
        }
    }

    override fun startCurrentActivity() {
        startActivity(Intent(this, CurrentActivity::class.java))
    }

    override fun showLoading() {
        progressBar.visibility = View.VISIBLE
        rvMusic.visibility = View.GONE
        tvDesc.visibility=View.GONE
    }

    override fun hideLoading() {
        progressBar.visibility = View.GONE
        rvMusic.visibility = View.VISIBLE
    }

    private fun showMessage(resId: Int) {
        Toast.makeText(this, resId, Toast.LENGTH_LONG).show()
    }

    override fun showMessageShortWord() {
        showMessage(R.string.music_search_short)
    }

    override fun showError() {
        showMessage(R.string.music_network_error)
    }

    override fun showMessageResultEmpty() {
        showMessage(R.string.music_no_results)
    }

    override fun initUI(result: DefaultRequest) {
        mAdapter.changeDataSet(result.results)
    }

}
