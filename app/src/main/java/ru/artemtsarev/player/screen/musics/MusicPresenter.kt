package ru.artemtsarev.player.screen.musics

import ru.artemtsarev.player.model.DefaultRequest
import ru.artemtsarev.player.model.Song
import ru.artemtsarev.player.repository.SongRepository
import javax.inject.Inject

class MusicPresenter @Inject
constructor(private val mView: MusicContract.View,
            private val mSongRepository: SongRepository
) : MusicContract.Presenter {

    override fun search(word: String) {
        if (word.length < 5) {
            mView.showMessageShortWord()
        } else {
            mSongRepository
                    .musics(word)
                    .doOnSubscribe { mView.showLoading() }
                    .doOnTerminate(mView::hideLoading)
                    .subscribe(
                            { result: DefaultRequest ->
                                if (result.results.isEmpty()) {
                                    mView.showMessageResultEmpty()
                                } else {
                                    mView.initUI(result)
                                }
                            }
                            ,
                            {
                                mView.showError()
                            })

        }
    }

    override fun clickSong(song: Song) {
        mSongRepository.current = song
        mView.playSong(song)
        mView.startCurrentActivity()
    }

}
