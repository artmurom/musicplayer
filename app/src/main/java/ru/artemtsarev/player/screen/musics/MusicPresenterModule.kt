package ru.artemtsarev.player.screen.musics

import dagger.Module
import dagger.Provides

@Module
class MusicPresenterModule(private val mView: MusicContract.View) {

    @Provides
    fun provideView(): MusicContract.View {
        return mView
    }
}
