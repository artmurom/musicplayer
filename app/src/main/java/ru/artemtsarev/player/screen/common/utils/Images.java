package ru.artemtsarev.player.screen.common.utils;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import ru.artemtsarev.player.R;


public final class Images {

    private Images() {
    }

    public static void loadImage(ImageView imageView, String url) {
        Picasso.get()
                .load(url)
                .transform(new CircleTransform())
                .noFade()
                .placeholder(R.drawable.ic_track)
                .into(imageView);
    }
}
