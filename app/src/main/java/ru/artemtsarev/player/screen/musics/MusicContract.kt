package ru.artemtsarev.player.screen.musics

import ru.artemtsarev.player.model.DefaultRequest
import ru.artemtsarev.player.model.Song


interface MusicContract {

    interface View {
        fun initUI(result: DefaultRequest)

        fun showMessageShortWord()

        fun playSong(song: Song)

        fun startCurrentActivity()

        fun showLoading()

        fun hideLoading()

        fun showMessageResultEmpty()

        fun showError()
    }

    interface Presenter {
        fun search(word: String)

        fun clickSong(song: Song)
    }
}
