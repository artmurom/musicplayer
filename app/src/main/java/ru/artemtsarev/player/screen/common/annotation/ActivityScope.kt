package ru.artemtsarev.player.screen.common.annotation

import javax.inject.Scope

@Scope
annotation class ActivityScope

