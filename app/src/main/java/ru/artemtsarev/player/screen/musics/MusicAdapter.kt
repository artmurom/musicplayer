package ru.artemtsarev.player.screen.musics

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_song.view.*
import ru.artemtsarev.player.R
import ru.artemtsarev.player.model.Song
import ru.artemtsarev.player.screen.common.utils.Images
import java.util.*


class MusicAdapter(private val mSongClickListner: OnSongClickListner)
    : RecyclerView.Adapter<MusicAdapter.GameHolder>() {

    private val mSongList: MutableList<Song>

    private val mListener = View.OnClickListener { v -> mSongClickListner.onSongClick(v.tag as Song) }

    init {
        mSongList = ArrayList()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GameHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_song, parent, false)
        return GameHolder(view)
    }

    override fun onBindViewHolder(holder: GameHolder, position: Int) {
        val song = mSongList[position]

        holder.setIsRecyclable(false)

        holder.itemView.tag = song
        holder.itemView.setOnClickListener(mListener)
        holder.bind(song)
    }

    fun changeDataSet(songList: List<Song>) {
        mSongList.clear()

        songList.filterTo(mSongList) { it.artistName != null && it.trackName != null && it.previewUrl != null && it.artworkUrl100 != null }

        notifyDataSetChanged()
        notifyItemChanged(1)
    }


    override fun getItemCount(): Int {
        return mSongList.size
    }

    class GameHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(song: Song) {
            Images.loadImage(itemView.ivTrack, song.artworkUrl100)

            itemView.tvTitle.text = song.trackName

            itemView.tvArtist.text = song.artistName

        }
    }

    interface OnSongClickListner {
        fun onSongClick(song: Song)
    }
}
