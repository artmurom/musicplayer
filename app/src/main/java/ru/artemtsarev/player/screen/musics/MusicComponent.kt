package ru.artemtsarev.player.screen.musics

import dagger.Subcomponent
import ru.artemtsarev.player.screen.common.annotation.ActivityScope

@ActivityScope
@Subcomponent(modules = [(MusicPresenterModule::class)])
interface MusicComponent {
    fun inject(musicActivity: MusicActivity)
}
