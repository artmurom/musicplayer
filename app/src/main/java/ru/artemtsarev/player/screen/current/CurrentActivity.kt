package ru.artemtsarev.player.screen.current

import android.content.ComponentName
import android.content.Intent
import android.content.ServiceConnection
import android.os.*
import android.support.v4.media.MediaMetadataCompat
import android.support.v4.media.session.MediaControllerCompat
import android.support.v4.media.session.PlaybackStateCompat
import android.support.v7.app.AppCompatActivity
import android.text.format.DateUtils
import android.widget.CheckBox
import android.widget.SeekBar
import kotlinx.android.synthetic.main.activity_current.*
import ru.artemtsarev.player.R
import ru.artemtsarev.player.screen.common.utils.Images
import ru.artemtsarev.player.service.MusicService
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledFuture
import java.util.concurrent.TimeUnit


class CurrentActivity : AppCompatActivity() {

    companion object {
        const val INTERNAL: Long = 1000
        const val INITIAL_INTERVAL: Long = 100
    }


    private var mBinder: MusicService.PlayerServiceBinder? = null
    private var mController: MediaControllerCompat? = null

    private var mSchedule: ScheduledFuture<*>? = null
    private val mExecutorService = Executors.newSingleThreadScheduledExecutor()

    private val mHandler = Handler()

    private var mLastPlaybackState: PlaybackStateCompat? = null


    private val mUpdateProgressTask = Runnable { updateProgress() }

    private val mServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            mBinder = service as MusicService.PlayerServiceBinder
            try {
                mController = MediaControllerCompat(this@CurrentActivity, mBinder!!.mediaSessionToken)
                connectToSession()
                mController!!.registerCallback(object : MediaControllerCompat.Callback() {
                    override fun onPlaybackStateChanged(state: PlaybackStateCompat?) {
                        mLastPlaybackState = state
                        if (state == null)
                            return
                        val playing = state.state == PlaybackStateCompat.STATE_PLAYING

                        cbPlayPause.isChecked = playing

                        if (playing) {
                            runSeekbarUpdate()
                        } else {
                            stopSeekbarUpdate()
                        }
                    }

                    override fun onMetadataChanged(metadata: MediaMetadataCompat?) {
                        if (metadata != null) {
                            updateUI(metadata)
                        }
                    }

                }

                )
            } catch (e: RemoteException) {
                mController = null
            }

        }

        override fun onServiceDisconnected(name: ComponentName) {
            mBinder = null
            mController = null
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_current)


        bindService(Intent(this, MusicService::class.java), mServiceConnection, BIND_AUTO_CREATE)


        cbPlayPause.setOnClickListener({
            val isChecked = (it as CheckBox).isChecked
            if (isChecked) {
                mController!!.transportControls.play()
                runSeekbarUpdate()
            } else {
                mController!!.transportControls.pause()
                stopSeekbarUpdate()
            }

        })


        sbProgress.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                tvProgress.text = DateUtils.formatElapsedTime((progress / 1000).toLong())
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                stopSeekbarUpdate()
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                val position = seekBar!!.progress
                mController!!.transportControls.seekTo((position).toLong())
                runSeekbarUpdate()
            }


        })
    }


    private fun connectToSession() {
        mLastPlaybackState = mController!!.playbackState
        if (mLastPlaybackState != null) {
            val playing = mLastPlaybackState!!.state == PlaybackStateCompat.STATE_PLAYING

            cbPlayPause.isChecked = playing

            if (playing) {
                runSeekbarUpdate()
            } else {
                stopSeekbarUpdate()
            }

            if (mController!!.metadata != null) {
                updateUI(mController!!.metadata)
            }
        }
    }

    public override fun onDestroy() {
        super.onDestroy()
        unbindService(mServiceConnection)
        stopSeekbarUpdate()
        mExecutorService.shutdown()
    }

    private fun runSeekbarUpdate() {
        stopSeekbarUpdate()
        if (!mExecutorService.isShutdown) {
            mSchedule = mExecutorService.scheduleAtFixedRate({
                mHandler.post(mUpdateProgressTask)
            }, INITIAL_INTERVAL, INTERNAL, TimeUnit.MILLISECONDS)
        }

    }

    private fun stopSeekbarUpdate() {
        if (mSchedule != null) {
            mSchedule!!.cancel(false)
        }
    }


    private fun updateUI(metadata: MediaMetadataCompat) {
        val duration = metadata.getLong(MediaMetadataCompat.METADATA_KEY_DURATION).toInt()
        sbProgress.max = duration
        tvDuration.text = DateUtils.formatElapsedTime((duration / 1000).toLong())

        Images.loadImage(imageView, metadata.getString(MediaMetadataCompat.METADATA_KEY_ART_URI))

        tvName.text = metadata.description.title
        tvArtist.text = metadata.description.subtitle
    }

    private fun updateProgress() {
        if (mLastPlaybackState == null) {
            return
        }
        var currentPosition = mLastPlaybackState!!.position
        if (mLastPlaybackState!!.state == PlaybackStateCompat.STATE_PLAYING) {
            val timeDelta = SystemClock.elapsedRealtime() -
                    mLastPlaybackState!!.lastPositionUpdateTime
            currentPosition += timeDelta * mLastPlaybackState!!.playbackSpeed.toLong()
        }
        sbProgress.progress = currentPosition.toInt()
    }
}
