package ru.artemtsarev.player.screen.splash

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ru.artemtsarev.player.screen.musics.MusicActivity


class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val intent = Intent(this, MusicActivity::class.java)
        startActivity(intent)
        finish()
    }
}






