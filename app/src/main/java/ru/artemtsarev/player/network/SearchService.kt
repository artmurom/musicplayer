package ru.artemtsarev.player.network

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import ru.artemtsarev.player.model.DefaultRequest

interface SearchService {

    @GET("search")
    fun search(@Query("term") word: String): Observable<DefaultRequest>

}