package ru.artemtsarev.player.model

import java.io.Serializable

data class Song(val artistName: String?, val trackName: String?, val artworkUrl100: String?, val previewUrl: String?) : Serializable {

}