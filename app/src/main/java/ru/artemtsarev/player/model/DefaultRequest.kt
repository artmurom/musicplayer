package ru.artemtsarev.player.model

import java.io.Serializable

data class DefaultRequest(val results: List<Song>) : Serializable