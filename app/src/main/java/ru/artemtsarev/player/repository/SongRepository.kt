package ru.artemtsarev.player.repository

import io.reactivex.Observable
import ru.artemtsarev.player.model.DefaultRequest
import ru.artemtsarev.player.model.Song


interface SongRepository {

    fun musics(word: String): Observable<DefaultRequest>

    var current: Song

}
