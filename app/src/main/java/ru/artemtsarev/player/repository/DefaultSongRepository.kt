package ru.artemtsarev.player.repository

import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.artemtsarev.player.model.DefaultRequest
import ru.artemtsarev.player.model.Song
import ru.artemtsarev.player.network.SearchService


class DefaultSongRepository(private val mSearchService: SearchService) : SongRepository {

    private lateinit var mCurrentSong: Song


    override fun musics(word: String): Observable<DefaultRequest> {
        return mSearchService
                .search(word)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }

    override var current: Song
        get() = mCurrentSong
        set(value) {
            mCurrentSong = value
        }

}
