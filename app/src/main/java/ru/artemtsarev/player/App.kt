package ru.artemtsarev.player

import android.app.Application
import ru.artemtsarev.player.di.AppComponent
import ru.artemtsarev.player.di.DaggerAppComponent
import ru.artemtsarev.player.di.DataModule


class App : Application() {

    override fun onCreate() {
        super.onCreate()

        component.inject(this)
    }

    val component: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .dataModule(DataModule(this))
                .build()
    }

}

